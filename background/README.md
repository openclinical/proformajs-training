## Preamble

Over the last few years Artificial Intelligence (AI) has become hugely popular with many predictions and projections that AI systems and tools will revolutionise healthcare. But what is AI? We are quite happy to take the Wikipedia definition (accessed 4/4/2021):

> “Leading AI textbooks define the field as the study of intelligent agents, that is any system that perceives its environment and takes actions that maximize its chance of successfully achieving its goals. Colloquially, the term "artificial intelligence" is often used to describe machines (or computers) that mimic [human] cognitive functions”

This course is intended to teach you a simple but effective and versatile way of developing AI services and agents. These may be as basic as recording and interpreting patient data or involve sophisticated decision making in complex situations where there are unpredictable events and many other kinds of uncertainty that are ubiquitous in healthcare.

When you are familiar with the methods you will be able to design and implement applications like clinical decision support systems and personalised, adaptive care pathways. We will show you how to build models for such decision tasks as risk assessment, diagnosis, treatment selection, drug prescribing and many other common clinical decisions. OPS models of care that look a little like “clinical algorithms” or workflow diagrams, but because they are executable the OPS engine can capture data and give patient-specific advice, and flexibly enact care pathways and ensuring that best practice is followed correctly and safely.

This basic course will get you started in designing, building and testing your AI application to provide patient specific guidance. The models may be developed purely for research purposes, as the basis of trials of proposed practice, or they might be routinely deployed to improve quality and safety of routine patient care.

As you become skilled in modelling you may wish to share the model with colleagues to facilitate discussion and debate, and we expect you will begin to invent new kinds of application. Whatever your motivation please consider submitting your application for publication on the OpenClinical repository ([www.openclinical.net](http://www.openclinical.net/)).

## The OpenClinical approach to medical AI and knowledge sharing  

Most of the AI systems that people talk about today focus on *data* (eg clinical data for a population of patients) and *learning* from the data; the goal is typically to build a classification or prediction tool that can be used to make decisions about an individual patient (eg whether there is a disease present and if so which) or predict a clinically significant event (eg whether the patient is likely to relapse or have an adverse event resulting from a drug).

This approach has achieved great success. Deep learning AI techniques accurately discover patterns in training data sets. But they normally produce black box models which do not explain their reasoning. Few health professionals are willing to take what can be life or death decisions without understanding how the predictions arise from the data in the specific case. Our approach is different. It provides the clinician with all of the evidence for or against each valid decision option. She can review these arguments and use her own clinical judgement to make the final choice.

Our approach was developed some years ago in a branch of AI called “knowledge engineering”. Our method is grounded in cognitive science which allows us to construct human-like models of expertise. In our experience these methods are intuitive and easy to understand, highly versatile in the kinds of tasks that can be implemented, and because they are human like they ensure that decision making and the reasons for recommendations or actions taken are transparent to the user.

## The basic ideas

### Knowledge representation  

![img](knowledge_ladder.png) A key idea in artificial intelligence is that of *knowledge representation*.  In order for a computer to be able to carry out processes that are “intelligent” in a way that it is intuitive it is necessary to formalise the knowledge that is central to human expertise in a way that a computer can use for decision-making, planning and so on.

A major part of this is the “domain knowledge” that a skilled practitioner such as a doctor has (eg. factual knowledge about diseases and their treatments) and also more general knowledge (e.g. for decision-making and planning). AI research has shown that knowledge can be viewed as a kind of ladder that represents increasingly sophisticated knowledge as illustrated by the diagram on the right.

The PRO*forma* modelling language provides the key concepts required for representing medical knowledge and data in this way, and OpenClinical provides a suite of tools to represent the key kinds of knowledge in the ladder.

At the bottom of the ladder are the domain-specific and more general symbols that everything else is built on. The simplest use is in *data modelling* and PRO*forma* supports most of the basic things that are needed for medical applications, including numbers, strings, sets, dates and times and so on.

These basic datatypes can be used to construct simple concept categories like the diseases or symptoms a patient may have. Concepts can be combined together into logical expressions to describe situations like “patient is male OR (patient is female AND patient is elderly)” using standard logical connectives (AND, OR, NOT).  Logical descriptions can be further combined to capture more complex concepts like rules, decisions and plans.  

The knowledge ladder reflects an important discovery in AI that knowledge is not just a blob of heterogeneous information but is organised in a way that captures our intuitive understanding of specialist expertise in terms of various kinds of reasoning and tasks in a form that can be understood by humans while also being executable on a computer. Logical descriptions for the patient or a situation can be combined to make logical rules.

#### PROforma basics

PRO*forma* is a language; like natural language it has a structure (syntax) and a meaning (semantics)[1](#sdfootnote1sym). It was designed for reasoning about data and situations and the decision-making and planning tasks required for creating executable models of specialist expertise. The language includes four classes of task which can be flexibly combined to model very simple models like prompts and reminders or complex decisions and care pathways.

![img](enquiry.png) An *enquiry* is a task to *seek information*. A simple case is presenting a form which asks the user to enter information about a patient’s age or gender or report the details of an abnormality on a mammogram. An enquiry may also be a point at which information is retrieved from a medical record or software is called to extract the required information from a digital image. A PRO*forma* model, however, only specifies the data that are required; the method for acquiring the data is determined separately. In OPS, the OpenClinical modelling tool, the symbol for a PRO*forma* enquiry is a green diamond.

![img](action.png) An even simpler kind of task is an *action*. Like an enquiry an action in a point in a process where a step in the model interacts with the “outside world” such as a message reminding the user to carry out a particular task or updating a database, or possibly using some sort of device which implements the action in the real world. Like enquiries the technical process is not specified by an action, only the point at which the operations are to be carried out. An action in OPS is represented by a blue square.

![img](decision.png) A more complex kind of task is a decision-making task where the PRO*forma* model helps the user to choose among a set of alternative interpretations of data (e.g. a diagnosis decision) or make a recommendation for care (e.g. selecting investigations or treatments).  Decision models are typically used to ensure that recommendations are individualised to reflect a patient’s circumstances or preferences. The OPS icon for a decision is a purple circle.

A key feature of PRO*forma* decision models is that they can deal with uncertainty, weighing up the evidence for alternative diagnoses or choosing the treatment that is most likely to benefit an individual patient. In PRO*forma* reasoning for decision making involves logical structures much like IF…THEN…  rules but they only increase or decrease confidence in the options unlike traditional logic which assumes something is true or false.

These are the main ideas behind the PRO*forma* language: data, descriptions, rules and decisions. As you can see from the knowledge ladder however just as we build descriptions out of data, rules out of descriptions and decisions out of logic applied to descriptions we can build at least two more kinds of knowledge from these elements: *plans* and *agents*.  

![img](plan.png) Plans are collections and sequences of tasks: enquiries, decisions and actions that can be executed in any desired order or reordered to reflect changing circumstances and assist a user in carrying out necessary tasks in a way that complies with clinical guidelines or follow a strict protocol like a research trial protocol. The icon for a plan in OPS is an orange rectangle as shown here.

#### An example: SOAP

To illustrate how we can use these ideas in modelling a process like a clinical guideline let's take the so-called “SOAP” mnemonic (Subjective, Objective, Assess and Plan) which is often used to structure a clinical note or guide a student in thinking about a case. But we can also think of SOAP as a PRO*forma* model, a tiny fragment of clinical expertise: first carry out enquiries to capture Subjective and Objective patient data then make an Assessment from the data to decide the best path to follow.

The figure below shows how we can model this process in OPS. Note that the whole network is a PRO*forma* plan but plans can also contain sub-plans and their component tasks. The SOAP example has a natural sequence indicated by the arrows: we first collect the data, then make the decision, and then depending on the decision carry out a single action or initiate a plan consisting of its own network of tasks.

![img](soap.png)

We will see later that tasks don't have to be carried out in a step by step manner but can start and stop as data are acquired or circumstances change, Furthermore any number of tasks can be executing simultaneously. A common feature of PRO*forma* models is that we may have two or 10 or 20 decisions active concurrently, monitoring the patient record and as the data arrive rules can “fire” and arguments are evaluated by the decisions and the decision options are instantaneously updated.

Despite its simplicity PRO*forma* has proved to be a very effective way of modelling complex clinical practises and reflects the everyday reality that specialist professionals such as clinicians have vast knowledge and countless skills which can be simultaneously in play. As tasks are completed, and decisions taken this can trigger another cascade of enquiries, decisions and tasks.

#### A cautionary note

it’s very easy to think of a PRO*forma* model as a step-by-step flowchart or clinical algorithm, which are quite inflexible models for care planning or modelling clinical guidelines. At every moment during the execution of a PRO*forma* model it is possible to arrange for the model to detect unexpected changes or adverse events. In response a PRO*forma* agent can simply notify the user but it can also change tack, changing the plan to ensure patient safety perhaps or even terminating the current plan and initiating a new one.  This kind of flexibility is exactly what you would expect from your doctor, why would you expect less of AI?  

#### What the future holds

A large proportion of developments in this area aim to develop “clinical decision support systems” which provide various kinds of assistance to their users, such as alerts and reminders, retrieving patient-specific questions, making recommendations etc. However the increasing pressures on front-line clinicians and growing acceptance of “virtual” services may lead to a need to be able to build agents that can run “autonomously” to reduce the burden of routine work. The analogy here is to autonomous robots like self-driving cars and possibly surgical robots.  When you have mastered PRO*forma* modelling you may want to experiment with autonomous services that can carry out routine tasks including simple decision making. The metaphor of the self-driving car is important here because safety is vital and even when a clinical service is operating autonomously the human clinician must be informed about, or able to , what the AI is doing and intervene at any time. We will explore how to build agents of this kind and some of the challenging issues surrounding them in a later course.

[1](#sdfootnote1anc) JAMIA (2003)
