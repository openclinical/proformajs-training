# PROforma Training course

*Building applications on OpenClinical*

* John Fox
* Omar Khan  (Omar.Khan@openclinical.net)
* Matt South (matt.south@openclinical.net)

## Introduction

This is intended to be a short course for modelling clinical processes in PRO*forma* using OPS (the Openclinical Publet System). Its intended to be just enough to get people started in as generic a manner as possible and sticks to the essentials of the language (data, task model etc).  It consists of four parts.

* [background](background)
* basic worked example
* guideline example
* advanced modelling
